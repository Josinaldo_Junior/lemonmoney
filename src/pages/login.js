import React, { Component } from 'react';
import {
  Alert,
  ErrorMessage,
  KeyboardAvoidingView,
  Image,
  Platform,
  StyleSheet,
  SafeAreaView,
  StatusBar,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from 'react-native';
import Lottie from 'lottie-react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import logo from '../assets/logo.png';
import coin from '../assets/coin.json'

export default class Login extends Component {
  componentDidMount() {
    this.animation.play();
  }

  static navigationOptions = {
    header: null,
  };
  state ={
    login: '',
    senha: '',
    error: '',
  };

  setLogin = (login) => {
    this.setState({login});
  };

  setSenha = (senha) => {
    this.setState({senha})
  };

  handleSignInPress = async () => {
    if(this.state.login.length === 0 || this.state.senha.length === 0){
      Alert.alert('All fields required')
    } else {
      Alert.alert('Fazendo o login');
      this.props.navigation.navigate('Main')
    }
  }
  render() {
    return (
      <SafeAreaView style={styles.container}> 
      
        <StatusBar
          barStyle="light-content"
          backgroundColor={styles.container.backgroundColor}
        />

        <Lottie
          ref={animation => {
            this.animation = animation;
          }}
          style={styles.lottieIcon}
          loop={true}
          source={coin}
        />
        
        <Image source={logo} style={styles.logo} />

        <KeyboardAvoidingView enabled={Platform.OS === 'android'} behavior="padding" style={styles.form}>
          <Text style={styles.label}>SEU E-MAIL *</Text>

          <TextInput
            style={styles.input}
            placeholder="Seu e-mail"
            value={this.state.login}
            onChangeText={this.setLogin}
            placeholderTextColor="#90ee90"
            keyboardType="email-address"
            autoCapitalize="none"
            autoCorrect={false}
          />

          <Text style={styles.label}>SUA SENHA *</Text>
          <TextInput
            style={styles.input}
            placeholder="Sua senha"
            value={this.state.senha}
            onChangeText={this.setSenha}
            placeholderTextColor="#90ee90"
            keyboardType='visible-password'
            autoCapitalize="none"
            autoCorrect={false}
          />
          {this.state.error.length !== 0 && <ErrorMessage>{this.state.error}</ErrorMessage>}
          <TouchableOpacity
            onPress={this.handleSignInPress}
            style={styles.button}>
            <Text style={styles.buttonText}>Login</Text>
          </TouchableOpacity>

          <TouchableOpacity>
            <View>
              <Text style={styles.esqueceuSenha}>Esqueceu sua senha?</Text>
            </View>
          </TouchableOpacity>

        </KeyboardAvoidingView>

      </ SafeAreaView >
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#3cb371'
  },

  form: {
    alignSelf: 'stretch',
    paddingHorizontal: 30,
    marginTop: 60,
  },

  label: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#fff',
    marginBottom: 8,
  },

  input: {
    borderWidth: 1,
    borderColor: '#ddd',
    paddingHorizontal: 20,
    fontSize: 16,
    color: '#fff',
    height: 44,
    marginBottom: 20,
    borderRadius: 2
  },

  logo: {
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 4
  },

  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 16,
  },

  button: {
    height: 42,
    backgroundColor: '#90ee90',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 2,
  },

  esqueceuSenha: {
    marginTop: 20,
    color: "#fff",
    backgroundColor: "transparent",
    textAlign: "center",
    paddingRight: 15,
    fontSize: 16
  },

  lottieIcon: {
    width: 100,
    height: 100,
    alignItems: 'center'
  }
  
});
