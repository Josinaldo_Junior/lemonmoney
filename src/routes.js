import { createAppContainer, createSwitchNavigator } from 'react-navigation'

import Main from './pages/main';
import Login from './pages/login';

const Routes = createAppContainer(
  createSwitchNavigator({
    Login,
    Main
  })
);

export default Routes;